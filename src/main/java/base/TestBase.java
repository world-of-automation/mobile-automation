package base;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static base.ExtentTestManager.getStackTrace;
import static base.ExtentTestManager.getTime;

public class TestBase {

    public static AppiumDriver appiumDriver = null;
    public static String platform = null;
    private static ExtentReports extent;

    /**
     * This method is about setting up the appium driver
     *
     * @param OS         operating system that you want the driver to run (Adnroid / iOS )
     * @param deviceName
     * @param version
     * @param udid
     * @param host
     * @param port
     * @return
     * @throws IOException
     */
    @Parameters({"OS", "deviceName", "version", "udid", "host", "port"})
    @BeforeMethod(alwaysRun = true)
    public static AppiumDriver setupDriver(String OS, String deviceName, String version, String udid,
                                           @Optional("localhost") String host, @Optional("4723") String port) throws IOException {
        platform = OS;
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
        cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, version);

        if (OS.equalsIgnoreCase("android")) {
            cap.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
            //use below appPackage, appActivity for TD
            cap.setCapability(MobileCapabilityType.APP_PACKAGE, "com.tdbank");
            cap.setCapability(MobileCapabilityType.APP_ACTIVITY, "com.td.dcts.android.us.app.SplashScreenActivity");
            //appPackage, appActivity --> appium inspector/ask dev
            //cap.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir") + "/src/main/resources/TD.apk");
            appiumDriver = new AndroidDriver(new URL("http://" + host + ":" + port + "/wd/hub"), cap);
        } else {
            cap.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.IOS);
            cap.setCapability(MobileCapabilityType.UDID, udid);
            cap.setCapability("bundleId", "com.example.apple-samplecode.UICatalog");
            //bundleId --> ask dev
            cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
            appiumDriver = new IOSDriver(new URL("http://" + host + ":" + port + "/wd/hub"), cap);
        }

        appiumDriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        return appiumDriver;
    }

    //Extent Report Setup
    @BeforeSuite(alwaysRun = true)
    public void extentSetup(ITestContext context) {
        ExtentTestManager.setOutputDirectory(context);
        extent = ExtentTestManager.getInstance();
    }

    @BeforeMethod(alwaysRun = true)
    public void startExtent(Method method) {
        String className = method.getDeclaringClass().getSimpleName();
        ExtentTestManager.startTest(method.getName());
        ExtentTestManager.getTest().assignCategory(className);
    }

    @AfterMethod(alwaysRun = true)
    public void afterEachTestMethod(ITestResult result) {
        ExtentTestManager.getTest().getTest().setStartedTime(getTime(result.getStartMillis()));
        ExtentTestManager.getTest().getTest().setEndedTime(getTime(result.getEndMillis()));
        for (String group : result.getMethod().getGroups()) {
            ExtentTestManager.getTest().assignCategory(group);
        }

        if (result.getStatus() == 1) {
            ExtentTestManager.getTest().log(LogStatus.PASS, "TEST CASE PASSED : " + result.getName());
        } else if (result.getStatus() == 2) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "TEST CASE FAILED : " + result.getName() + " :: " + getStackTrace(result.getThrowable()));
        } else if (result.getStatus() == 3) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "TEST CASE SKIPPED : " + result.getName());
        }
        ExtentTestManager.endTest();
        extent.flush();
        if (result.getStatus() == ITestResult.FAILURE) {
            ExtentTestManager.captureScreenshot(appiumDriver, result.getName());
        }
    }

    @AfterSuite(alwaysRun = true)
    public void generateReport() {
        extent.close();
    }

    @AfterMethod(alwaysRun = true)
    public void cleanUp() {
        appiumDriver.quit();
    }


    /**
     * This method will swipe either up, Down, left or Right according to the
     * direction specified. This method takes the size of the screen and uses
     * the swipe function present in the Appium driver to swipe on the screen
     * with a particular timeout. There is one more method to implement swipe
     * using touch actions, which is not put up here.
     *
     * @param Direction The direction we need to swipe in.
     * @param swipeTime The swipe time, ie the time for which the driver is supposed
     *                  to swipe.
     * @param Offset    The offset for the driver, eg. If you want to swipe 'up', then
     *                  the offset is the number of pixels you want to leave from the
     *                  bottom of the screen t start the swipe.
     * @Author -
     * @Modified By -
     */

    public void functionSwipe(String Direction, int swipeTime, int Offset) {
        //for .swipe() you need appium java client 3.1.0
        Dimension size;
        size = (appiumDriver).manage().window().getSize();
        int starty = (int) (size.height * 0.80);
        int endy = (int) (size.height * 0.20);
        int startx = size.width / 2;
        if (Direction.equalsIgnoreCase("Up")) {
            ((AppiumDriver<WebElement>) (appiumDriver)).swipe(startx / 2, starty - Offset, startx / 2, endy, swipeTime);
        } else if (Direction.equalsIgnoreCase("Down")) {
            ((AppiumDriver<WebElement>) (appiumDriver)).swipe(startx / 2, endy + Offset, startx / 2, starty, swipeTime);
        } else if (Direction.equalsIgnoreCase("Right")) {
            starty = size.height / 2;
            endy = size.height / 2;
            startx = (int) (size.width * 0.10);
            int endx = (int) (size.width * 0.90);
            ((AppiumDriver<WebElement>) (appiumDriver)).swipe(startx + Offset, starty, endx, endy, swipeTime);
        } else if (Direction.equalsIgnoreCase("Left")) {
            starty = size.height / 2;
            endy = size.height / 2;
            startx = (int) (size.width * 0.90);
            int endx = (int) (size.width * 0.10);
            ((AppiumDriver<WebElement>) (appiumDriver)).swipe(startx - Offset, starty, endx, endy, swipeTime);
        }
    }

    /**
     * This method will make the driver wait
     *
     * @param seconds wait time in seconds
     */
    public void sleepFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException ignored) {
        }
    }

    public boolean validateElementIsDisplayedOrExist(WebElement element) {
        boolean flag;
        try {
            flag = element.isDisplayed();
        } catch (NoSuchElementException ee) {
            flag = false;
        }
        return flag;
    }

    public WebElement scrollTillFound(WebElement element) {
        for (int i = 0; i < 5; i++) {
            try {
                element.isDisplayed();
                break;
            } catch (NoSuchElementException e) {
                functionSwipe("UP", 200, 200);
            }
        }
        return element;
    }

    public void scrollTillClickAbleAndClick(WebElement element) {
        for (int i = 0; i < 5; i++) {
            try {
                element.click();
                break;
            } catch (NoSuchElementException e) {
                functionSwipe("UP", 200, 200);
            }
        }
    }

}
