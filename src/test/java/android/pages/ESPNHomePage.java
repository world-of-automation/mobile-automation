package android.pages;

import base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ESPNHomePage extends TestBase {

    @FindBy(xpath = "//android.widget.Button[@text='SIGN UP']")
    private WebElement signUpBtn;

    @FindBy(id = "com.android.packageinstaller:id/permission_allow_button")
    private WebElement allowBtn;


   /* @iOSXCUITFindBy(xpath = "")
    @AndroidFindBy(xpath = "")
    private WebElement findByTests;

       // only version 7+

    public void testFindBy(){
        findByTests.click();
    }*/

    public void clickOnAllow() {
        allowBtn.click();
    }

    public void clickOnSignUp() {
        signUpBtn.click();
    }
}
