package android.pages;

import base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class TDHomePage extends TestBase {

    @FindBy(xpath = "//android.widget.Button[@text='ACCOUNTS']")
    private WebElement accountBtn;

    @FindBy(xpath = "//android.widget.TextView[@text='View Accounts']")
    private WebElement viewAccountBtn;

    @FindBy(xpath = "//android.widget.Button[@text='TRANSFER']")
    private WebElement transferBtn;

    @FindBy(xpath = "//android.widget.Button[@text='DEPOSIT']")
    private WebElement depositBtn;

    @FindBy(xpath = "//android.widget.Button[@text='SEND MONEY']")
    private WebElement sendMoneyBtn;

    @FindBy(xpath = "//android.widget.Button[@text='PAY A BILL']")
    private WebElement payABillBtn;

    @FindBy(xpath = "//android.widget.ImageButton[@content-desc='Go Back']")
    private WebElement backBtn;

    @FindBy(xpath = "//android.widget.TextView[@text='Products']")
    private WebElement productsBtn;

    @FindBy(xpath = "//android.widget.TextView[@text='© 2020 TD Bank, N.A. All Rights Reserved.']")
    private WebElement copyrightText;

    public void clickOnAccount() {
        accountBtn.click();
    }

    public void validateTextMatches() {
        String actual = accountBtn.getText();
        String expected = "ACCOUNT";
        Assert.assertEquals(actual, expected);
    }

    public void testSwipe() {
        functionSwipe("Up", 200, 200);
        sleepFor(3);
        functionSwipe("Down", 200, 200);
    }

    public void clickAndGoBackToAllHeaders() {
        accountBtn.click();
        backBtn.click();
        transferBtn.click();
        backBtn.click();
        depositBtn.click();
        backBtn.click();
        sendMoneyBtn.click();
        backBtn.click();
        payABillBtn.click();
        backBtn.click();
    }


}
