package android.pages;

import base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TDLoginPage extends TestBase {

    @FindBy(xpath = "//android.widget.EditText[@text='User name']")
    private WebElement userNameField;

    @FindBy(xpath = "//android.widget.EditText[@text='Password']")
    private WebElement passwordField;

    @FindBy(xpath = "//android.widget.Button[@text='Log in']")
    private WebElement loginBtn;

    public void fillUpUserData() {
        userNameField.sendKeys("testuser@gmail.com");
        passwordField.sendKeys("test2734e");
        loginBtn.click();
    }

}
