package android.testcases;

import android.pages.TDHomePage;
import android.pages.TDLoginPage;
import base.ExtentTestManager;
import base.TestBase;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TDTestClass extends TestBase {

    TDHomePage tdHomePage = null;
    TDLoginPage tdLoginPage = null;

    @BeforeMethod
    public void setup() {
        tdHomePage = PageFactory.initElements(appiumDriver, TDHomePage.class);
        tdLoginPage = PageFactory.initElements(appiumDriver, TDLoginPage.class);
    }


    @Test
    public void validateHeaderButtonsAreWorking() {
        tdHomePage.clickAndGoBackToAllHeaders();
    }

    @Test
    public void validateUserBeingAbleToSwipe() {
        sleepFor(5);
        tdHomePage.testSwipe();
        ExtentTestManager.log("swiped", TDTestClass.class);
        sleepFor(5);
    }


}
