package android.testcases;

import android.pages.ESPNHomePage;
import base.TestBase;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class ESNPTestClass extends TestBase {

    /**
     * Jira Story : WBA-22134
     * User should be able to sign up using valid required information
     * This test should run on dev env only.
     * Yet to be checked on QA env
     */
    @Test
    public void userBeingAbleToSignUp() {
        ESPNHomePage espnHomePage = PageFactory.initElements(appiumDriver, ESPNHomePage.class);
        sleepFor(5);
        espnHomePage.clickOnAllow();
        sleepFor(3);
        espnHomePage.clickOnAllow();
        sleepFor(3);
        espnHomePage.clickOnSignUp();
        sleepFor(5);
    }
}
